﻿using MediatR;
using my_app_backend.Application.QueryRepositories;
using my_app_backend.Domain.AggregateModel.BookAggregate.Events;
using Newtonsoft.Json;

namespace my_app_backend.Application.EventHandlers
{
    public class BookUpdateEventHandler : INotificationHandler<BookUpdatedEvent>
    {
        private readonly IBookRepository _bookRepository;
        private readonly ILogger<BookUpdateEventHandler> _logger;

        public BookUpdateEventHandler(IBookRepository bookRepository, ILogger<BookUpdateEventHandler> logger)
        {
            _bookRepository = bookRepository;
            _logger = logger;
        }

        public async Task Handle(BookUpdatedEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var rs = await _bookRepository.GetById(notification.BookId);

                if (!rs.IsSuccessful)
                {
                    throw new Exception(rs.Message);
                }

                var book = rs.Data;

                book.Name = notification.Name;
                book.Author = notification.Author;
                book.Type = notification.Type;
                book.Locked = notification.Locked;

                var updateRs = await _bookRepository.Update(book);
                if (!updateRs.IsSuccessful)
                {
                    throw new Exception(updateRs.Message);
                }
            }
            catch (Exception ex)
            {
                _logger.Equals($"Exception happened: sync to read repository fail for BookQuantityUpdatedEvent: {JsonConvert.SerializeObject(notification)}, ex: {ex}");
                throw;
            }
        }
    }
}