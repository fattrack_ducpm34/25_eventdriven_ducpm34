﻿using MediatR;
using my_app_backend.Application.QueryRepositories;
using my_app_backend.Domain.AggregateModel.BookAggregate.Events;
using Newtonsoft.Json;

namespace my_app_backend.Application.EventHandlers
{
    public class BookDeletedEventHandler : INotificationHandler<BookDeletedEvent>
    {
        private readonly IBookRepository _bookRepository;
        private readonly ILogger<BookDeletedEventHandler> _logger;

        public BookDeletedEventHandler(IBookRepository bookRepository, ILogger<BookDeletedEventHandler> logger)
        {
            _bookRepository = bookRepository;
            _logger = logger;
        }

        public async Task Handle(BookDeletedEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var rs = await _bookRepository.GetById(notification.BookId);

                if (!rs.IsSuccessful)
                {
                    throw new Exception(rs.Message);
                }

                var book = rs.Data;

                if (book.Locked)
                {
                    _logger.LogError("Book is locked. Cannot delete");
                    throw new ArgumentException("Book is locked.Cannot delete");
                }

                var deleteRs = await _bookRepository.Delete(notification.BookId);

                if (!deleteRs.IsSuccessful)
                {
                    throw new Exception(deleteRs.Message);
                }
            }
            catch (Exception ex)
            {
                _logger.Equals($"Exception happened: sync to read repository fail for DeletedBook: {JsonConvert.SerializeObject(notification)}, ex: {ex}");
                throw;
            }
        }
    }
}